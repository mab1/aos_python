# Python Scripts to talk to AOS

## Example 1: Execute a Graph Query

```
(venv2_7) mab@Mehdis-MacBook-Pro-2 ~/mab_lab/aos_python (master) $ python aos_graph_query.py 
----------------------------------------------------------------------
 AOS server:        172.20.94.3
 Blueprint Label:   MAB_3_Stage_BP_1
 Graph Query:       match(node('system', name='leaf', role='spine'))
----------------------------------------------------------------------
 Logging to AOS server ..
     POST:  https://172.20.94.3/api/aaa/login
     Token: *********************OBQHRV_XMA

 Get Blueprint ID ..
     GET:   https://172.20.94.3/api/blueprints
     BP ID: d7b6c386-218e-402b-9f14-0583b720a996

 Query graph ..
     POST:   https://172.20.94.3/api/blueprints/d7b6c386-218e-402b-9f14-0583b720a996/qe?type=deployed
----------------------------------------------------------------------
{'count': 4,
 'items': [{'leaf': {'deploy_mode': None,
                     'group_label': None,
                     'hostname': 'spine1',
                     'id': 'b5e89f72-4d7b-4fc9-bfcc-1c433a4cee4f',
                     'label': 'spine1',
                     'position_data': {'plane': 0,
                                       'pod': 0,
                                       'position': 0,
                                       'region': 0},
                     'property_set': None,
                     'role': 'spine',
                     'system_id': None,
                     'system_type': 'switch',
                     'tags': None,
                     'type': 'system'}},
           {'leaf': {'deploy_mode': None,
                     'group_label': None,
                     'hostname': 'spine4',
                     'id': 'e69dcc2f-03f2-413b-ad5c-55c2f665445e',
                     'label': 'spine4',
                     'position_data': {'plane': 0,
                                       'pod': 0,
                                       'position': 3,
                                       'region': 0},
                     'property_set': None,
                     'role': 'spine',
                     'system_id': None,
                     'system_type': 'switch',
                     'tags': None,
                     'type': 'system'}},
           {'leaf': {'deploy_mode': None,
                     'group_label': None,
                     'hostname': 'spine3',
                     'id': 'f44f8b86-f11f-46d0-925a-c48d3e6180b4',
                     'label': 'spine3',
                     'position_data': {'plane': 0,
                                       'pod': 0,
                                       'position': 2,
                                       'region': 0},
                     'property_set': None,
                     'role': 'spine',
                     'system_id': None,
                     'system_type': 'switch',
                     'tags': None,
                     'type': 'system'}},
           {'leaf': {'deploy_mode': None,
                     'group_label': None,
                     'hostname': 'spine2',
                     'id': 'bfe87987-e841-4f05-82d5-ad7543ff881a',
                     'label': 'spine2',
                     'position_data': {'plane': 0,
                                       'pod': 0,
                                       'position': 1,
                                       'region': 0},
                     'property_set': None,
                     'role': 'spine',
                     'system_id': None,
                     'system_type': 'switch',
                     'tags': None,
                     'type': 'system'}}]}

----------------------------------------------------------------------
 Logout from AOS server ..
     POST:  https://172.20.94.3/api/user/logout

----------------------------------------------------------------------
(venv2_7) mab@Mehdis-MacBook-Pro-2 ~/mab_lab/aos_python (master) $ 
```