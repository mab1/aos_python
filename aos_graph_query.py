# ----------------------------------------------------------------------------
# Modules Import
# ----------------------------------------------------------------------------
import json
from pprint import pprint
import requests
import warnings

# ----------------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------------
aos_address = '172.20.94.3'
aos_username = "admin" 
aos_password = "admin"
blueprint_label = 'MAB_3_Stage_BP_1'
graph_query_expression = "match(node('system', name='leaf', role='spine'))"

aos_base_api = "https://" + aos_address + "/api"
authorization_header = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}

# ----------------------------------------------------------------------------
# Functions: REST CRUD operations
# ----------------------------------------------------------------------------
def rest_api_post(active_url, active_headers, active_body):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        rest_response = requests.post(url=active_url, headers=active_headers, \
            data=json.dumps(active_body), verify=False)
        return rest_response


def rest_api_get(active_url, active_headers):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        rest_response = requests.get(url=active_url, headers=active_headers, \
            verify=False)
        return rest_response


# ----------------------------------------------------------------------------
# Functions: AOS operations
# ----------------------------------------------------------------------------
def aos_login(address, username, password):
    api_endpoint = '/aaa/login'
    api_url = aos_base_api + api_endpoint
    headers = authorization_header
    body = {
        'username': username,
        'password': password
    }
    print(' ' + 'Logging to AOS server ..')
    print('     ' + 'POST:  ' + api_url)
    rest_response = rest_api_post(active_url=api_url, active_headers=headers, \
        active_body=body)
    if rest_response.status_code == 201:
        json_parsed_response = json.loads(rest_response.content)
        session_token = json_parsed_response['token']
        print('     ' + 'Token: *********************' + session_token[-10:])
        print('')
        return session_token
    else:
        return (False, "Failed!")


def aos_logout(address, token):
    api_endpoint = '/user/logout'
    api_url = aos_base_api + api_endpoint
    body = {}
    authorization_header['AUTHTOKEN'] = token
    headers = authorization_header
    print(' ' + 'Logout from AOS server ..')
    print('     ' + 'POST:  ' + api_url)
    print('')
    print('-'*70)
    rest_response = rest_api_post(active_url=api_url, active_headers=headers, \
        active_body=body)


def get_blueprint_id(address, token, blueprint_label):
    api_endpoint = '/blueprints'
    api_url = aos_base_api + api_endpoint
    authorization_header['AUTHTOKEN'] = token
    headers = authorization_header
    print(' ' + 'Get Blueprint ID ..')
    print('     ' + 'GET:   ' + api_url)
    rest_response = rest_api_get(active_url=api_url, active_headers=headers)
    if rest_response.status_code == 200:
        blueprints = json.loads(rest_response.content)['items']
        for blueprint in blueprints:
            for k, v in blueprint.items():
                if k == 'label' and v == blueprint_label:
                    blueprint_id = blueprint['id']
                    print('     ' + 'BP ID: ' + blueprint_id)
                    print('')
                    return blueprint_id
    else:
        return (False, "Failed!")
        print('')


def get_graph_query_result(address, token, blueprint_id, graph_query):
    api_endpoint = '/blueprints/' + blueprint_id + '/qe?type=deployed'
    api_url = aos_base_api + api_endpoint
    body = {
        'query': graph_query
    }
    authorization_header['AUTHTOKEN'] = token
    headers = authorization_header
    print(' ' + 'Query graph ..')
    print('     ' + 'POST:   ' + api_url)
    print('-'*70)
    rest_response = rest_api_post(active_url=api_url, active_headers=headers,\
        active_body=body)
    if rest_response.status_code == 200:
        response = json.loads(rest_response.content)
        response_count = json.loads(rest_response.content)['count']
        response_items = json.loads(rest_response.content)['items']
        pprint(response)
        print('')
        print('-'*70)
    else:
        return (False, "Failed!")
        print('')


def print_environement():
    print('-'*70)
    print(' ' + 'AOS server:        ' + aos_address)
    print(' ' + 'Blueprint Label:   ' + blueprint_label)
    print(' ' + 'Graph Query:       ' + graph_query_expression)
    print('-'*70)

# ----------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------
if __name__ == '__main__':
    print_environement()
    aos_login_session = aos_login(aos_address, aos_username, aos_password)
    blueprint_id = get_blueprint_id(aos_address, aos_login_session, blueprint_label)
    get_graph_query_result(aos_address, aos_login_session, blueprint_id, graph_query_expression)
    aos_logout(aos_address, aos_login_session)
